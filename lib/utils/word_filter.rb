# This module provides the capability to find words in a string and replace them
# with the defined replacement character.
#
# Usage:
#
#   phrase = "My favorite band is Queen"
#   blacklist = "queen"
#
#   filter(phrase, blacklist)
#
# Output:
#
#   "My favorite band is *****"
#
module WordFilter

  REPLACEMENT = "*"

  def filter(phrase, blacklist)
    phrase.tap do |str|
      each_word(blacklist).each { |word| str.gsub!(regex(word), replace(word)) }
    end
  end

  private

    def each_word(blacklist)
      blacklist.split
    end

    def replace(word)
      REPLACEMENT * word.size
    end

    def regex(word)
      /\b#{word}\b/i
    end

end
