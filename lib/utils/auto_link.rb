# This module provides the capability to turn usernames i.e @rbmrclo into links.
#
# Usage:
#
#   message = "Hello @luke, I would like to introduce to you @skywalker."
#   domain  = "http://github.com"
#
#   link_to_users(message, domain)
#
# Output:
#
#   "Hello <a href='http://github.com/luke'>@luke</a>, I would like to
#   introduce to you <a href='http://github.com/skywalker'>@skywalker</a>."
#
#
module AutoLink

  # Looks for an @ sign that is not supposed to be a username i.e "rbmrclo@hotmail.com"
  # Allows a username with @ sign at the beginning
  REGEX = /(?<!\w)@(\w+)/

  def link_to_users(message, domain)
    message.tap do |str|
      extracted_users(message).each do |user|
        str.gsub!("@#{user}", autolink(domain, user))
      end
    end
  end

  private

    def extracted_users(message)
      message.scan(REGEX).flatten
    end

    def autolink(domain, user)
      %Q{<a href='#{normalize_url(domain)}/#{user}'>@#{user}</a>}
    end

    # Ensures that the url doesn't have trailing backslashes
    def normalize_url(domain)
      domain.sub(/(\/)+$/,'')
    end

end
