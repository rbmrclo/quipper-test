# This module provides the capability to score word occurrences in a string.
#
# Usage:
#
#   content = "The quick brown fox jumps over the lazy dog"
#
#   tally(content)
#
# Output:
#
#   {
#     the:   2,
#     quick: 1,
#     brown: 1,
#     fox:   1,
#     jumps: 1,
#     over:  1,
#     lazy:  1,
#     dog:   1
#   }
#
module WordCounter

  REGEX = /[\p{Alpha}\-']+/

  def tally(words)
    each_word(words).each_with_object(Hash.new(0)) do |word, hash|
      hash[format(word)] += 1
    end
  end

  private

    def each_word(words)
      words.scan(REGEX)
    end

    # Make sure that the word is hash[key] friendly
    def format(word)
      word.downcase.to_sym
    end

end
