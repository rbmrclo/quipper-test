require_relative 'utils/word_counter'
require_relative 'utils/word_filter'
require_relative 'utils/auto_link'

module WordHelper
  extend WordCounter
  extend WordFilter
  extend AutoLink
end
