# 1. Remove Car's explicit reference to Engine by using dependency injection.

require 'engine'

class Car

  attr_accessor :engine

  def initialize(engine)
    @engine = engine
  end

  def move
    engine.accelerate
  end

end
