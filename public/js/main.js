$(function(){

  $("#about").click(function(e){
    is_active(e.currentTarget);
    rotate(0);
  });

  $("#vision").click(function(e){
    is_active(e.currentTarget);
    rotate(90);
  });

  $("#mission").click(function(e){
    is_active(e.currentTarget);
    rotate(180);
  });

  $("#contact").click(function(e){
    is_active(e.currentTarget);
    rotate(270);
  });

  function rotate(degrees) {
    $("#cube").css("transform", "rotateY(-" + degrees + "deg)");
  }

  function is_active(anchor) {
    $li = $(anchor).parent()
    $(".navbar li").removeClass('active')
    $li.addClass('active')
  }

});
