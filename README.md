# Quipper Test

- Name: Robbie Marcelo
- Github: [@rbmrclo](https://github.com/rbmrclo)

### Test 1A: Problem
Write a module called 'WordHelper' and write code to make the following specs pass.
```ruby
describe "WordHelper" do

  it "tally words in a string" do
    sample = "key tree cat mouse tree monkey"

    expected = {
      key: 1,
      tree: 2,
      cat: 1,
      mouse: 1,
      monkey: 1
    }

    expect(
      WordHelper.tally(sample)
    ).to eq(expected)
  end

  it "applies blacklist" do
    blacklist =  %{count before}
    phrase = "Don't couNt your Chickens before they Hatch"

    expected = "Don't ***** your Chickens ****** they Hatch"

    expect(
      WordHelper.filter(phrase, blacklist)
    ).to eq(expected)
  end

  it "creates link to user" do
    domain = "http://github.com/"
    message = "Hi @jack and @jill please review my commit."

    expected = "Hi <a href='http://github.com/jack'>@jack</a> and <a href='http://github.com/jill'>@jill</a> please review my commit."

    expect(
      WordHelper.link_to_users(message, domain)
    ).to eq(expected)
  end
end
```
### Test 1A: Solution

- Available at: [`lib/word_helper.rb`](https://bitbucket.org/rbmrclo/quipper-test/src/9c4ce7299e1568ede4666a2d481ad701368df371/lib/word_helper.rb)
- Run the tests via: `rspec spec/lib/word_helper.rb`

### Test 1B: Problem
1. Remove Car's explicit reference to Engine by using dependency injection.
2. Spec the Car class, in isolation.

```ruby
class Car
  attr_reader :engine

  def engine
    @engine ||= Engine.new(4)
  end

  def move
    engine.accelerate
  end
end

class Engine
  attr_reader :cylinders

  def initialize(cylinders)
    @cylinders = cylinders
  end

  def accelerate

  end
end
```

### Test 1B: Solution

- Available at: [`lib/car.rb`](https://bitbucket.org/rbmrclo/quipper-test/src/9c4ce7299e1568ede4666a2d481ad701368df371/lib/car.rb)
- Run the tests via: `rspec spec/lib/car_spec.rb`

### Test 2: Problem
```html
<html>
<!--

Quipper are creating a site to promote a new product called HyperCube. Currently it's
looking very bland, grey and not very interactive, so the product manager has asked that:

1. The navbar be styled to match the colour scheme and style of the one at www.quipper.com (Quipper blue is #18bde6)
2. OpenSans be used as the main font, with nice crisp titles rather than heavy bold ones.
3. The 4 panels be replaced with some kind of centered carousel. He would especially like if the carousel transitioned with a CSS3 3D cube rotation.
4. The navbar menu to include 4 links for each of the 4 faces of the carousel.

-->
    <head>
        <title>HyperCube</title>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <style>
            .faces {
                display: block;
                margin: 0;
                padding: 0;
            }
            .faces > li {
                display: block;
                list-style: none;
                background: #f0f0f0;
                padding: 10px;
                margin-bottom: 10px;
                float: left;
                width: 230px;
                height: 200px;
                margin-right: 5px;
            }
            .faces > li > h2 {
                font-size: 20px;
                margin: 0 0 5px 0;
            }

            /* You can either put your CSS here or link to an external file. */

        </style>

        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            // You can either put your JavaScript here or link to an external file.
        </script>
    </head>

    <body>

        <div class="navbar">
            <a class="navbar-brand" href="#">HyperCube&trade;</a>
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
            </ul>
        </div>

        <div class="container">

            <ul class="faces">
                <li class="face">
                    <h2>What is HyperCube&trade;?</h2>
                    <p>
                        The HyperCube&trade; is one of Quipper's most fascinating cube projects,
                        combining facets of the standard square and cube, while allowing for a
                        staggering <em>n</em> dimensions!
                    </p>
                </li>
                <li class="face">
                    <h2>Imagine the possibilities!</h2>
                    <p>
                        Where the limits of standard cubes end, the endless possibilities of
                        hypercubes begin! Prepare yourself to step into a whole new dimension.
                    </p>
                </li>
                <li class="face">
                    <h2>How is it possible?</h2>
                    <p>
                        Since it is so difficult to imagine an <em>n</em>-dimensional object in a 3D space,
                        let alone the flat plane of this very website, we will represent hypercubes
                        as if they were standard cubes.
                    </p>
                </li>
                <li class="face">
                    <h2>Is there more?</h2>
                    <p>
                        If you would like to know more about the incredible benefits of installing
                        HyperCube&trade; in your Infinite Display System then please call us
                        for a quote and let's see what we can do for you!
                    </p>
                </li>
            </ul>

        </div>

        <div class="container">
            HyperCube&trade; &mdash; It's like a cube, but <em>Hyper</em>!
        </div>
    </body>
</html>
```

### Test 2: Solution
- Available at: [`public/index.html`](https://bitbucket.org/rbmrclo/quipper-test/src/9c4ce7299e1568ede4666a2d481ad701368df371/public/index.html)