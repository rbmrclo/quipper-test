# 2. Spec the Car class, in isolation.

require_relative '../spec_helper'

describe Car do

  let(:engine) { Engine.new(4) }
  let(:car)    { Car.new(engine) }

  describe '#initialize' do
    it 'creates an instance of Engine' do
      expect(car.instance_variable_get :@engine).to be_a Engine
    end
  end

  describe '#move' do
    it 'accelerates the engine' do
      expect(car.engine).to receive(:accelerate)

      car.move
    end
  end

end
